// Mock Database
let posts = [];

// post ID
let count = 1;


// Reactive DOM with JSON (CRUD Operation)


// ADD POST DATA

document.querySelector("#form-add-post").addEventListener('submit', (e) => {
	// Prevents the default behavior of an event
	// To prevent the page from reloading (default of behavior of submit)
	e.preventDefault();
	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});
	count++
	console.log(posts);
	alert("Post Successfully Added")
	showPosts()

});

// RETRIEVE POST
const showPosts = () => {
	let postEntries = "";
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})
	console.log(postEntries);
	document.querySelector("#div-post-entries").innerHTML = postEntries
}



// EDIT POST (Edit Button)
const editPost = (id) => {

	// The function first uses the querySelector() method to get the element with the id "#post-title-${id}"
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title
	document.querySelector("#txt-edit-body").value = body;
}


// Update POST (update Button)
document.querySelector("#form-edit-post").addEventListener('submit', (e) =>{
	e.preventDefault();
	for (let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			showPosts(posts);
		}
	}
})


//delete Post
const deletePost = (id) => {
	for (let i = 0; i < posts.length; i++){
		if (posts[i].id.toString() === id){
			
			
			posts.splice(i,1)
			const element = document.querySelector(`#post-${id}`);
			element.remove();

			/*delete posts[i];
			console.log(posts)*/
			alert("Post Successfully Deleted")
			showPosts(posts);
			
		}
	}
}